﻿
using System.Linq;
using System.Net.Http.Headers;
using UnityEngine;
using UnityEngine.AI;

public enum FSMState
{
    ENTER,
    UPDATE,
    EXIT,
}

public abstract class AState
{
    protected FSMState state;
    protected AState nextState;
    protected NPC_Controller npc;

    protected AState( NPC_Controller npc)
    {
        this.state = FSMState.ENTER;
        this.npc = npc;
    }

    protected virtual void StateEnter() { state = FSMState.UPDATE; }
    protected virtual void StateUpdate() { }
    protected virtual void StateExit() { }

    public AState Process()
    {
        switch (state)
        {
            case FSMState.ENTER:
                StateEnter();
                break;
            case FSMState.UPDATE:
                StateUpdate();
                break;
            case FSMState.EXIT:
                StateExit();
                return nextState;
        }

        return this;
    }
}

public class Run : AState
{
    public Run(NPC_Controller npc) : base(npc)
    {
    }

    protected override void StateEnter()
    {
        Debug.Log("Run start");
        npc.SetColor(Color.yellow);
        base.StateEnter();
    }
    protected override void StateUpdate()
    {
        Debug.Log("Run Update");
        if (!npc.Agent.hasPath || npc.Agent.remainingDistance < 1)
        {
            var randomIndex = Random.Range(0, npc.WpList.Count);
            var destination = npc.WpList[randomIndex];
            npc.Agent.SetDestination(destination.position);
        }
        if(npc.Hunter)
        {
            nextState = new Haunt(npc);
            state = FSMState.EXIT;
        }
    }

    protected override void StateExit()
    {
        Debug.Log("Run Exit");
        base.StateExit();
    }
}

public class Haunt : AState
{
    float updateDestinationTime = 0.1f;
    float currentTime = 0;
    public Haunt(NPC_Controller npc ) : base(npc)
    {
    }

    protected override void StateEnter()
    {
        Debug.Log("Haunt start");
        npc.SetColor(Color.red);
        base.StateEnter();
    }
    protected override void StateUpdate()
    {
        Debug.Log("Haunt update");
        if (currentTime < updateDestinationTime)
        {
            currentTime += Time.deltaTime;
        }
        else
        {
            currentTime = 0;
            var list = GameManager.Instance.Players.OrderBy(p => Vector3.Distance(npc.transform.position, p.PlayerTransform.position)).ToList();
            npc.Agent.SetDestination(list[1].PlayerTransform.position);
        }
        if(!npc.Hunter)
        {
            nextState = new Run(npc);
            state = FSMState.EXIT;
        }
    }

    protected override void StateExit()
    {
        Debug.Log("Haunt exit");
        base.StateExit();
    }

}