﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class NPC_Controller : MonoBehaviour, IMinigamePlayer
{
    [SerializeField] private Transform _wpContainer;
    List<Transform> _wpList = new List<Transform>();
    public List<Transform> WpList => _wpList;

    NavMeshAgent _agent;
    public NavMeshAgent Agent => _agent;

    public Transform PlayerTransform => this.transform;

    private AState currentState;

    private bool _hunter = false;
    public bool Hunter => _hunter;

    MeshRenderer _meshRenderer;

    private void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();
        foreach(Transform t in _wpContainer)
        {
            _wpList.Add(t);
        }
        _meshRenderer = GetComponentInChildren<MeshRenderer>();
        _meshRenderer.material.color = Color.yellow;
        currentState = new Run(this);
    }

    // Update is called once per frame
    void Update()
    {
        currentState = currentState.Process();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(_hunter && other.gameObject.TryGetComponent<IMinigamePlayer>(out IMinigamePlayer _player))
        {
            _player.Touch();
            _hunter = false;
        }
    }

    public void Touch()
    {
        StartCoroutine(setHaunterCoroutine());
        Debug.Log("got hunted");
    }
    IEnumerator setHaunterCoroutine()
    {
        yield return new WaitForSeconds(0.3f);
        _hunter = true;
    }

    public void SetColor(Color _color)
    {
        _meshRenderer.material.color = _color;
    }
}

public interface IMinigamePlayer
{
    Transform PlayerTransform { get; }
    void Touch();
}