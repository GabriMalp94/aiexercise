﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }


    [SerializeField] List<IMinigamePlayer> _players;
    public List<IMinigamePlayer> Players => _players;

    private int startingIndex;
    private void Awake()
    {
        if(!Instance)
        {
            Instance = this;
            _players = FindObjectsOfType<MonoBehaviour>().OfType<IMinigamePlayer>().ToList();
        }
        else
        {
            Destroy(this.gameObject);
        }
        startingIndex = Random.Range(0, _players.Count);
    }

    private void Start()
    {
        Players[startingIndex].Touch();
    }
}
