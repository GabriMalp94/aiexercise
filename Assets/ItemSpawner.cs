﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
    [SerializeField] GameObject item;
    [SerializeField] private Transform _pContainer;
    List<Transform> _wpList = new List<Transform>();

    public float time = 5;
    private float currentTime = 0;

    private void Awake()
    {
        foreach (Transform t in _pContainer)
        {
            _wpList.Add(t);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(currentTime < time)
        {
            currentTime += Time.deltaTime;
        }
        else
        {
            currentTime = 0;
            int randomindex = Random.Range(0, _wpList.Count);
            Instantiate(item, _wpList[randomindex].position, Quaternion.identity);
        }
    }
}
