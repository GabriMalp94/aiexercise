﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class PlayerController : MonoBehaviour
{
    private NavMeshAgent _agent;
    private int score;

    private void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Mouse1))
        {
            var worldClickedPoint = Camera.main.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast(worldClickedPoint, out RaycastHit hit))
            {
                Debug.DrawLine(Camera.main.transform.position, hit.point, Color.red, 2f);
                _agent.SetDestination(hit.point);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Fire")
        {
            Destroy(this.gameObject);
        }
    }
}
